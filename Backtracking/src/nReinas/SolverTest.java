package nReinas;

import static org.junit.Assert.*;
import org.junit.Test;

public class SolverTest {

	@Test (expected=IllegalStateException.class)
	public void testCantSolucionesSinResolver() {
		Solver s = new Solver(8);
		s.cantSoluciones();
	}

	@Test (expected=IllegalStateException.class)
	public void testSolucionesSinResolver() {
		Solver s = new Solver(8);
		assertSolucionesCorrectas(s);
	}

	@Test
	public void testCantSoluciones8reinas() {
		Solver s = new Solver(8);
		s.resolver();
		assertEquals(92,s.cantSoluciones());
	}

	@Test
	public void testSoluciones8reinas() {
		Solver s = new Solver(8);
		s.resolver();
		assertSolucionesCorrectas(s);
	}

	@Test
	public void testSoluciones10reinas() {
		Solver s = new Solver(10);
		s.resolver();
		assertSolucionesCorrectas(s);
	}

	@Test
	public void testSoluciones6reinas() {
		Solver s = new Solver(6);
		s.resolver();
		assertSolucionesCorrectas(s);
	}

	private void assertSolucionesCorrectas(Solver s) {
		for (Solucion sol: s)
			solucionCorrecta(sol);
	}

	private void solucionCorrecta(Solucion sol) {
		int fila;
		for (int col=1; col<=sol.tamano(); col++) {
			fila = sol.nroFilaOcupada(col);
			sol.quitarReina(col);
			assertFalse(sol.casillaAmenazada(col,fila));
			sol.agregarReina(col,fila);
		}
	}

}
