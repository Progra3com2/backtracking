package nReinas;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SolucionTest {

	Solucion tablero;
	
	@Before
	public void setUp() throws Exception {
		tablero = new Solucion(8);
	}

	@Test
	public void dosReinasFila1test() {
		tablero.agregarReina(1, 1);
		assertTrue(tablero.casillaAmenazada(2,1));
	}

	@Test
	public void dosReinasJuntasEnDiagonal() {
		tablero.agregarReina(1, 1);
		assertTrue(tablero.casillaAmenazada(2,2));
	}

	@Test
	public void dosReinasNoAmenazadas() {
		tablero.agregarReina(1, 1);
		assertFalse(tablero.casillaAmenazada(2,3));
	}

	@Test
	public void dosReinasAmenazadasExtremosDiagonal() {
		tablero.agregarReina(1, 1);
		assertTrue(tablero.casillaAmenazada(8,8));
	}

	@Test
	public void dosReinasAmenazadasExtremosFila() {
		tablero.agregarReina(1, 4);
		assertTrue(tablero.casillaAmenazada(8,4));
	}

}
