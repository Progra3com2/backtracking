package nReinas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Solver implements Iterable<Solucion> {

	private int _tamano;
	private Solucion _actual;
	private List<Solucion> _soluciones;
	
	public Solver(int n) {
		_tamano = n;
	}
	
	public int tamano() {
		return _tamano;
	}
	
	public void resolver() {
		_actual = new Solucion(_tamano);
		_soluciones = new ArrayList<Solucion>();
		generarDesde(1);
	}

	private void generarDesde(int col) {
		if (col>tamano()) {
			// caso base: completamos todas las columnas, encontramos una solucion
			_soluciones.add(_actual.clone());
		}
		else {
			// recursivamente agregamos reinas en las columnas siguientes
			for (int fila=1; fila<=tamano(); fila++) {
				if (!_actual.casillaAmenazada(col,fila)) {
					_actual.agregarReina(col,fila);
					generarDesde(col+1);
					_actual.quitarReina(col);
				}
			}
		}
	}

	public int cantSoluciones() {
		validarEstado();
		return _soluciones.size();
	}

	@Override
	public Iterator<Solucion> iterator() {
		validarEstado();
		return _soluciones.iterator();
	}

	private void validarEstado() {
		if (_soluciones==null)
			throw new IllegalStateException("Debe ejecutarse primero el metodo resolver()");
	}

}
