package nReinas;

import static java.lang.Math.*;

public class Solucion {

	private int[] _reinas;
	
	public Solucion(int n) {
		_reinas = new int[n];
	}

	public int tamano() {
		return _reinas.length;
	}
	
	public void agregarReina(int col, int fila) {
		validarFilaCol(col,fila);
		_reinas[col-1]=fila;
	}
	
	public void quitarReina(int col) {
		validarColumna(col);
		_reinas[col-1]=0;
	}

	public int nroFilaOcupada(int col) {
		return _reinas[col-1];
	}

	public boolean casillaAmenazada(int col, int fila) {
		validarFilaCol(col,fila);

		for (int i=1; i<=tamano(); i++) {
			if (!columnaVacia(i) &&
					(nroFilaOcupada(i)==fila || enDiagonal(col, fila, i, nroFilaOcupada(i)))) {
				return true;
			}
		}
		return false;
	}

	private boolean columnaVacia(int col) {
		return _reinas[col-1]==0;
	}
	
	private boolean enDiagonal(int col1, int fila1, int col2, int fila2) {
		return abs(col1-col2)==abs(fila1-fila2);
	}

	public Solucion clone() {
		Solucion ret = new Solucion(tamano());
		for (int col=1; col<=tamano(); col++)
			ret.agregarReina(col, nroFilaOcupada(col));
		return ret;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		for (int i=1; i<=tamano(); i++)
			sb.append(" ").append(nroFilaOcupada(i)).append(",");
		return sb.deleteCharAt(sb.length()-1).append(" ]").toString();
	}
	
	private void validarFilaCol(int col, int fila) {
		validarColumna(col);
		validarFila(fila);
	}

	private void validarColumna(int col) {
		if (!validarIndice(col))
			throw new IllegalArgumentException("Indice de columna fuera de rango: "+col);
	}

	private void validarFila(int fila) {
		if (!validarIndice(fila))
			throw new IllegalArgumentException("Indice de fila fuera de rango: "+fila);
	}
		
	private boolean validarIndice(int i) {
		return i>0 && i<=tamano();
	}
}
