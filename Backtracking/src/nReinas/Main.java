package nReinas;

public class Main {

	public static void main(String[] args) {
		int[] tamanos = {6,8,10};
		
		for (int n: tamanos) {
			Solver s = new Solver(n);
			s.resolver();
			imprimirSoluciones(s);
		}

	}

	public static void imprimirSoluciones(Solver s) {
		System.out.println("Cantidad de soluciones para " + s.tamano() + " reinas: "
	                       + s.cantSoluciones());
		for (Solucion sol: s)
			System.out.println(sol);
		System.out.println();
	}
	
}
